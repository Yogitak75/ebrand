import { Component, OnInit } from '@angular/core';

export interface MenuItem {
  label: string;
  // icon: string;
}
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {


  menuItems: MenuItem[] = [
    {
      label: 'Dashboard'
     
    },
    {
      label: 'Available Themes',
  
    },
    {
      label: 'Website List',
      
    },
    {
      label: 'Manage Orders',
      
    },
    
    
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
